﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FirstCoreMVCApplication.Models;

namespace FirstCoreMVCApplication.Controllers
{
  public class HomeController : Controller
  {
    private readonly IStudentRepository _repository = null;

    public HomeController(IStudentRepository repository)
    {
      _repository = repository;
    }

    public JsonResult GetStudentDetails(int Id)
    {
      //TestStudentRepository repository = new TestStudentRepository();
      Student studentDetails = _repository.GetStudentById(Id);
      return Json(studentDetails);
    }
  }
}
