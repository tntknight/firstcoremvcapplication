﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirstCoreMVCApplication.Models
{
  public class Student
  {
    public int StudentId { get; set; }
    public String Name { get; set; }
    public String Branch { get; set; }
    public String Section { get; set; }
    public String Gender { get; set; }
  }
}
